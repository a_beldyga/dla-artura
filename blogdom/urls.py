from django.conf.urls import patterns, include, url
from django.views.generic import ListView


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',


    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^login/$', 'fullblog.views.authentication', name='login'),
    url(r'^logout/$', 'fullblog.views.lout', name='wyloguj'),
    url(r'^preferencje/$', 'fullblog.views.preferencje', name='preferencje'),
    url(r'^blog/user/(?P<user>[\w\-]+)', 'fullblog.views.mojWpis', name='mojWpis'),
    url(r'^blog/add', 'fullblog.views.add', name='news'),
    url(r'^blog/', 'fullblog.views.blog', name='blog'),
#    url(r'^blog/user', 'fullblog.views.wpisUsera', name='user_wpis'),








)
