from django import forms
from django.contrib.auth.models import User
from models import UserProfile
from django.contrib.auth import authenticate

class Form(forms.Form):
    user = forms.CharField(max_length=100)
    title = forms.CharField(max_length=1000)
    content=forms.CharField(max_length=100000)


class CanvasForm(forms.ModelForm):
    login = forms.CharField(max_length=100)
    canvasPassword = forms.CharField(max_length=100)

    class Meta:
        model = UserProfile
        fields = ('login', 'canvasPassword')


class AuthForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(max_length=100)

    def clean(self):
        username = self.data.get('username')
        password = self.data.get('password')
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            raise forms.ValidationError("Sorry, that login was invalid. Please try again.")
        return self.data

    def login(self, request):
        username = self.data.get('username')
        password = self.data.get('password')
        user = authenticate(username=username, password=password)
        return user


