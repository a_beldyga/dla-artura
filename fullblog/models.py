from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


# Create your models here.
class Dane(models.Model):
    user = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    content=models.CharField(max_length=1000)
    published=models.DateTimeField()

    # On Python 3: def __str__(self):
    def __unicode__(self):
        return self.title


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    login = models.CharField(max_length=200)
    canvasPassword = models.CharField(max_length=200)
    flaga = models.BooleanField(default =False)


    #other fields here

    def __str__(self):
          return "%s's profile" % self.user

def create_user_profile(sender, instance, created, **kwargs):
    if created:
       profile, created = UserProfile.objects.get_or_create(user=instance)

post_save.connect(create_user_profile, sender=User)
