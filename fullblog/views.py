# -*- coding: utf-8 -*-
# Create your views here.
from django.shortcuts import render_to_response, redirect
from models import *
from form import *
from django.core.context_processors import csrf
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.shortcuts import render
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required


def blog(request):
    newsy= Dane.objects.all().order_by('-pk')
    users = User.objects.all()
    return render(request, 'base.html', {'newsy': newsy, 'users': users})


def add(request):

    if request.method == 'GET':
            c = {}
            c.update(csrf(request))
            return render_to_response('add.html', c)
    if request.method == 'POST':
            form = Form(request.POST) # A form bound to the POST data
            user = form.data['user']
            title = form.data['title']
            content = form.data['text']

            r = Dane(user=user, title=title, content=content,  published= timezone.now())
            r.save()
            return redirect(reverse('blog'))


    else:
        return redirect(reverse('blog'))



# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def lout(request):
    logout(request)
    return redirect(reverse('blog'))

def mojWpis(request, user="p6"):

  #  user=User.objects.get(id=request.user.id)
    wpisy=Dane.objects.order_by('-pk')
    wpisyUsera=[]
    for wpis in wpisy:
        if wpis.user==user:
            wpisyUsera.append(wpis)

    return render_to_response('wpisyUsera.html',{'wpisy':wpisyUsera, 'user':user})


def authentication(request):
    if request.method == 'GET':
        c = {}
        c.update(csrf(request))
        return render_to_response('login.html', c)
    if request.method == 'POST':
        form=AuthForm(request.POST)
        if form.is_valid():
            # the password verified for the user
            user = form.login(request)
            if user.is_active:
                print("User is valid, active and authenticated")
                login(request, user)
                return redirect(reverse('blog'))
            else:
                print("The password is valid, but the account has been disabled!")
        else:
            # the authentication system was unable to verify the username and password
            print("The username and password were incorrect.")
            return render_to_response('login.html', {'form': form}, context_instance=RequestContext(request))

def preferencje(request):


    if request.method == 'GET':
            c = {}
            c.update(csrf(request))
            return render_to_response('preferencje.html', c)
    if request.method == 'POST':
            form = CanvasForm(request.POST) # A form bound to the POST data
            user=User.objects.get(id=request.user.id)
            login = form.data['login']
            canvasPassword = form.data['canvasPassword']


            r = UserProfile(user=user, login = login, canvasPassword = canvasPassword)
            r.save()
            print r.login
            print r.canvasPassword
            print r.user.id
            return redirect(reverse('blog'))


    else:
        return redirect(reverse('blog'))


